export default function toNum(str){

	const arr = [...str]

	// console.log(arr)

	const filteredArr = arr.filter(element => {

		return element !== ","
	})

	return parseInt(filteredArr.reduce((x,y)=>{

		return x + y

	}))
}