import Head from 'next/head'
import styles from '../styles/Home.module.css'

import {Fragment,useState,useEffect,useContext} from 'react'
import {DropdownButton,Dropdown,Container,Row,Col,Form,Button,Table} from 'react-bootstrap'
import Swal from 'sweetalert2'

import Router from 'next/router'

import UserContext from '../userContext'

export default function Categories(){

	const {user} = useContext(UserContext)

	const [firstName,setFirstName] = useState("")
	const [lastName,setLastName] = useState("")
	const [balance,setBalance] = useState("")

	const [categoryName,setCategoryName]=useState("")
	const [type,setType]=useState("")	

	const [categories,setCategories]=useState([])
	const [categoriesList,setCategoriesList] = useState([])

	const [recordDescription,setRecordDescription] = useState("")
	const [recordCategory,setRecordCategory] = useState("")
	const [recordType, setRecordType] = useState("")
	const [recordAmount, setRecordAmount] = useState("")
	const [isActive,setIsActive] = useState(true)

	useEffect(()=>{

		if(type !=="" && categoryName !==""){

			setIsActive(true)
		} else {

			setIsActive(false)
		}

	},[type, categoryName])		
	

	function addCategory(e){

		e.preventDefault()

		fetch('https://afternoon-inlet-64537.herokuapp.com/api/users/category', {
			method: 'POST',
			headers: { 
				'Content-Type':'application/json',
				'Authorization':`Bearer ${(localStorage.getItem('token'))}`
			},
			body: JSON.stringify({
				id: user.id,
				name: categoryName,
				type: type
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data === true){
				Swal.fire({
					icon: "success",
					title: "Category added",
				})
				Router.reload()
				setCategoryName("")
				setType("")


				} else {
				Swal.fire({
					icon: "error",
					text: "Something went wrong"
				})
			}		

		})			

	}	

   	useEffect(()=>{

		fetch('https://afternoon-inlet-64537.herokuapp.com/api/users/allCategories', {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}` 
			}
		})
		.then(res => res.json())
		.then(data => {
			
			setCategories(data)
	   		const categoryOptions = []
			data.forEach(cat => {				
				if(recordType === "Expense" && cat.type === "Expense"){
					categoryOptions.push(cat)
				} else if(recordType === "Income" && cat.type === "Income"){
					categoryOptions.push(cat)
				}

			})

			setCategoriesList(categoryOptions)
			setRecordCategory("")
		})   		


   	},[recordType])


	const transactionTypes = [
		{
		    id: 1,
		    value: 'Income'
		}, {
		    id: 2,
		    value: 'Expense'
		}
	]

	

	//display all user categories
		const categoryTable = categories.map(option => {
			return (
				    <tr key={option.name}>
      					<td>{option.name}</td>
      					<td>{option.type}</td>
				    </tr>
				)
		})

    	const transactions = transactionTypes.map(option => {
       		return(
	            <option key={option.id} value={option.value}>                                   
	            	{option.value}
				</option>
       		)
       })


	// console.log(type)
	return (
	
		<Container>
			
			<Row>
				<Col md={4}>				
					<div className={styles.card}>
						<Form onSubmit={e => addCategory(e)}>
							<Form.Group controlId="categoryName">
								<h5 className="text-center mt-2 mb-4">Add New Category</h5>
								<Form.Control type="text" placeholder="Name" value={categoryName} onChange={e => setCategoryName(e.target.value)} required/>
							</Form.Group>
							<Form.Group>
								<Form.Control as="select" value={type} onChange={e => setType(e.target.value)}> 
									<option id="transactionMarker" value="" disabled>- Select Transaction Type</option>
									{transactions}
								</Form.Control>
							</Form.Group>
							<a href="./profile" className="col-md-3 offset-6 text-center warning-outline">&larr; Back to Profile        </a>
							{
							isActive
							? <Button className="working-button" type="submit" variant="danger">Add</Button>
							: <Button variant="danger" disabled>Add</Button>
							}
						</Form>
					</div>
					<img class="mt-4" src="/steps.svg" alt="Register" height="220px"/>
				</Col>
				<Col md={8}>
					<div className={styles.card}>
						<h2 className="text-center">Category List</h2>				
						<Table striped bordered hover>
							<thead>
								<tr>
									<th>Category Name</th>
									<th>Transaction Type</th>
								</tr>
							</thead>
							<tbody>
								{categoryTable}
							</tbody>
						</Table>
					</div>										
				</Col>
			</Row>			

			</Container>

	)
}


