import Head from 'next/head'
import styles from '../styles/Home.module.css'
import moment from 'moment'
import numbersWithCommas from '../helpers/commas'
import toNum from '../helpers/toNum'
import {DropdownButton,Dropdown,Container,Row,Col,Form,Card,InputGroup,FormControl,Button} from 'react-bootstrap'
import {Fragment,useState,useEffect,useContext} from 'react'

import Swal from 'sweetalert2'

import Router from 'next/router'

import UserContext from '../userContext'

export default function Transactions(){

	const {user} = useContext(UserContext)	

	const [option,setOption] = useState("All")

	const [keyword,setKeyword] = useState("")

	const [displayArr,setDisplayArr] = useState([])

	const [optionOutline,setOptionOutline] = useState("secondary")


   	useEffect(()=>{

		fetch('https://afternoon-inlet-64537.herokuapp.com/api/users/allTransactions', {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}` 
			}
		})
		.then(res => res.json())
		.then(data => {
			
			let displayArr = []
			let searchResults = []
			let basket = []

			data.reverse()
			
			basket = data.filter(res => {

				let find = res.description.toLowerCase().includes(keyword.toLowerCase())
				return find
			})			
			

			setDisplayArr(basket)

			if(option === "Expense"){
				searchResults = basket.filter(res => res.type === "Expense")
				setOptionOutline("danger")
				setDisplayArr(searchResults)
			} else if (option === "Income"){
				searchResults = basket.filter(res => res.type === "Income")
				setOptionOutline("success")
				setDisplayArr(searchResults)
			} else {
				setOptionOutline("secondary")
			}
		})   		

   	},[option,keyword])

   	function selectOption(e){
   		setOption(e)
   	}

   	const display = displayArr.map(content =>{

   		let variant, borderColor
   		let	price = numbersWithCommas(content.amount)
   		let	date = moment(content.date).format('L')
   		let	balance = numbersWithCommas(content.balance)

   		if(content.type == "Income"){
   			variant = "text-success h5"
   			borderColor = "success"
   		} else {
   			variant = "text-danger h5"
   			borderColor = "danger"
   		}


		return (
			<Card key={content._id} border={borderColor} className={styles.card}>				
				<Card.Body>
					<Row>
						<Col md={3}>
							<Card.Title>
								<h4>{content.description}</h4>
							</Card.Title>					
						</Col>
						<Col>							
							<Card.Text className="text-muted">
								{content.category}
							</Card.Text>						
						</Col>
						<Col>							
							<Card.Text>
								{content.type}
							</Card.Text>
						</Col>
						<Col>
							<Card.Text className={variant}>
								 {price}
							</Card.Text>						
						</Col>
						<Col md={3}>
							<Card.Text className="text-muted">
								{date}
							</Card.Text>							
						</Col>
					</Row>
				</Card.Body>
			</Card>
		)   		
   	})

	return (
		<Fragment>
			<h2 className="text-center">Transaction Records</h2>
			<Container b={4}>
				<Row>
					<Col md={{span: 8, offset: 2}}>						
						<InputGroup className="my-4">
							<Form.Control type="text" placeholder="Search" value={keyword} onChange={e => setKeyword(e.target.value)} required/>
							<DropdownButton
						    as={InputGroup.Append}
						    variant={optionOutline}			      
							title={option}
							menuAlign="right"
							onSelect={e => selectOption(e)}>
								<Dropdown.Item eventKey="All">All</Dropdown.Item>
								<Dropdown.Divider />
								<Dropdown.Item eventKey="Income">Income</Dropdown.Item>
								<Dropdown.Item eventKey="Expense">Expense</Dropdown.Item>
						    </DropdownButton>
						</InputGroup>						
					</Col>
				</Row>	
				{display}
			<p className={styles.description}>
                <code className={styles.code}><b>End of Results</b></code>
                <br/>
                <br/>
                <br/>
            </p>
			</Container>
		</Fragment>
	)
}