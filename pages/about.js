import Head from 'next/head'
import {Fragment,useEffect,useState,useContext} from 'react'
import styles from '../styles/Home.module.css'
import Router from 'next/router'
import {Container,Row,Col,Form,Button} from 'react-bootstrap'

import UserContext from '../userContext'

export default function About() {

    const [email,setEmail] = useState("")
    const [message,setMessage] = useState("")
    const [isActive,setIsActive] = useState(true)

    useEffect(()=>{

        if(email !== "" && message !==""){

            setIsActive(true)

        } else {

            setIsActive(false)
        }

    },[email,message])    

    function sendMessage(e){

    }

    return(

        <Fragment>          

            <div className={styles.container}>
              <main className={styles.main}>
                <h1>About the Site</h1>

                <p className={styles.description}>
                <code className={styles.code}><b>Pocket Gem</b> is an online budget tracker</code>
                </p>
                <p className={styles.description}>
                <code className={styles.code}>created with MongoDB, Express, ReactJS, NextJS and NodeJS</code>
                </p>
                
                <Col md={8}>                    
                  <div className={styles.card}>
                    <h3 className="text-center">The Developer</h3>
                    <a className="text-center" href="https://www.linkedin.com/in/genevieve-manzanilla-34261796/"
                        target="_blank"
                        rel="noopener noreferrer">
                        <h5 className="text-danger"><b>Genevieve Manzanilla</b></h5>
                    </a>                    

                  </div>
                </Col>

         
              </main> 
            </div>         
        </Fragment> 


        )
}

                    // <Form onSubmit={e => sendMessage(e)}>
                    //     <Form.Group controlId="email">
                    //         <Form.Control type="email" placeholder="Email" value={email} onChange={e => setEmail(e.target.value)} required>
                    //         </Form.Control>
                    //     </Form.Group>
                    //     <Form.Group controlId="message">
                    //         <Form.Control type="text" rows="3" placeholder="Message" value={message} onChange={e => setMessage(e.target.value)} required>
                    //         </Form.Control>
                    //     </Form.Group>       
                    //     {
                    //         isActive
                    //         ? <Button variant="danger" className="btn-block mb-1" type="submit">Send</Button>
                    //         : <Button variant="danger" className="btn-block mb-1" disabled>Send</Button>
                    //     }                        
                    // </Form>

