import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/globals.css'
import {Fragment} from 'react'
import Head from "next/head"
import{useState,useEffect} from 'react'
import NavBar from '../components/NavBar'
import Footer from '../components/Footer'
import {UserProvider} from '../userContext'
import {Container} from 'react-bootstrap'

function MyApp({ Component, pageProps }) {

  const [user,setUser] = useState({

  	email: null,
    id: null

  })

  useEffect(()=>{

  	setUser({
  		email: localStorage.getItem('email'),
      id: localStorage.getItem('id')
  	})
  },[])

  const unsetUser = () => {

  	localStorage.clear()

  	setUser({

  		email:null,
      id: null
  	})

  }  

  return (
  	<Fragment>
      <Head>
        <title>Pocket Gem</title>
        <link rel="icon" href="/gem.ico" />
      </Head>
	  	<UserProvider value={{user,setUser,unsetUser}}>
	  		<NavBar />
        <Container>
          <Component {...pageProps} />
        </Container>	
	  	</UserProvider>
  	</Fragment>

  	)

}

export default MyApp
