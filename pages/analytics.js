import {useState,useEffect} from 'react'
import BarChart from '../components/VerticalBar'
import LineGraph from '../components/LineGraph'
import {Row,Col,Container} from 'react-bootstrap'
import moment from 'moment'
import styles from '../styles/Home.module.css'

export default function Analytics(){

	return(
		
		<Container >
			<h1 className="text-center">Analytics</h1>
			<div className={styles.card}>					
				<BarChart />
			</div>
			<div className={styles.card}>
				<LineGraph />
			</div>
		</Container>
		
		)

}