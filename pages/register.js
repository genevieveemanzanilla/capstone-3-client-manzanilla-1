import {useEffect,useState,useContext} from 'react'
import {Container,Row,Col,Form,Button} from 'react-bootstrap'
import styles from '../styles/Home.module.css'

import Router from 'next/router'

import Swal from 'sweetalert2'

export default function Register(){

	const [firstName,setFirstName] = useState("")
	const [lastName,setLastName] = useState("")
	const [email,setEmail] = useState("")
	const [password1,setPassword1] = useState("")
	const [password2,setPassword2] = useState("")

	const [isActive,setIsActive] = useState(true)

	useEffect(()=>{

		if((firstName !=="" && lastName !=="" && email !=="" && password1 !=="" && password2 !=="") && (password1 === password2)){

			setIsActive(true)

		} else {

			setIsActive(false)
		}

	},[firstName,lastName,email,password1,password2])

	function registerUser(e){

		e.preventDefault()

		let initialCategories = [
			{"name": "Payroll Salary", "type":"Income"},
			{"name": "Business Profits", "type":"Income"},
			{"name": "Food", "type":"Expense"},
			{"name": "Transportation", "type":"Expense"},
			{"name": "Utilities", "type":"Expense"},
			{"name": "Leisure", "type":"Expense"}
		]

		fetch('https://afternoon-inlet-64537.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email
			})
			
		})
		.then(res => res.json())
		.then(data => {

			if(data === false){

				fetch('https://afternoon-inlet-64537.herokuapp.com/api/users', {
					method: 'POST',
					headers: { 'Content-Type': 'application/json' },
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						categories: initialCategories
					})
				})
				.then(res => res.json())
				.then(data => {

					if(data === true){
						Swal.fire({

							icon: "success",
							title: "Successfully Registered.",
							text: "Thank you for registering"
						})
						Router.push("/login")
					} else {
						Swal.fire({

							icon: "error",
							title: "Unsuccessful register",
							text: "Something went wrong"
						})
					}

				})

			} else {

					Swal.fire({

						icon: "error",
						title: "Something went wrong",
						text: "Email already exists"
					})				

			}

		})	

		setFirstName("")
		setLastName("")
		setEmail("")
		setPassword1("")
		setPassword2("")

		Swal.fire({

			icon: "success",
			title: "Successfully Registered.",
			text: "Thank you for registering"
		})
	}

	return (
		<Container className={styles.grid}>
			<Row>				
				<Col className={styles.card}>				
					<h1 class="text-center">Register</h1>
					<Form onSubmit={e => registerUser(e)}>
						<Form.Row>							
							<Col>
								<Form.Group controlId="userFirstName">
									<Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
								</Form.Group>								
							</Col>
							<Col>								
								<Form.Group controlId="userLastName">
									<Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={e => setLastName(e.target.value)} required/>
								</Form.Group>
							</Col>						
						</Form.Row>
						<Form.Group controlId="userEmail">
							<Form.Control type="email" placeholder="Enter Email" value={email} onChange={e => setEmail(e.target.value)} required/>
						</Form.Group>
						<Form.Row>							
							<Col>
								<Form.Group controlId="userPassword1">
							<Form.Control type="password" placeholder="Enter Password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
								</Form.Group>								
							</Col>
							<Col>								
								<Form.Group controlId="userPassword2">
							<Form.Control type="password" placeholder="Confirm Password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
								</Form.Group>
							</Col>						
						</Form.Row>						
						<Form.Row>
							<Col md={9}>
								<a href="./profile" className="inline-center">&larr; Back to Login</a>								
							</Col>							
							<Col md={3}>								
								{
									isActive
									?
									<Button className="mt-2" variant="danger" type="submit">Submit</Button>
									:
									<Button className="mt-2" variant="danger" disabled>Submit</Button>										
								}						
							</Col>
						</Form.Row>
					</Form>
					<img class="mt-4" src="/registerImage.svg" alt="Register" height="220px"/>				
				</Col>
			</Row>
		</Container>
		)
}
