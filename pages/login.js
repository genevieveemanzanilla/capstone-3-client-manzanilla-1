import {useState,useEffect,useContext} from 'react'
import {Container,Row,Col,Carousel,Form,Button} from 'react-bootstrap'
import Swal from 'sweetalert2'
import styles from '../styles/Home.module.css'
import Router from 'next/router'

import UserContext from '../userContext'

import {GoogleLogin} from 'react-google-login'

export default function Login(){

	const {user,setUser} = useContext(UserContext)
	const [email,setEmail] = useState("")
	const [password,setPassword] = useState("")
	const [isActive,setIsActive] = useState(true)

	useEffect(()=>{

		if(email !=="" && password !==""){

			setIsActive(true)
		} else {

			setIsActive(false)
		}

	},[email,password])	

	function authenticate(e){

		e.preventDefault()

		fetch('https://afternoon-inlet-64537.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: { 'Content-Type':'application/json'},
			body: JSON.stringify({
				email: email,
				password: password
			})

		})
		.then(res => res.json())
		.then(data => {

			if (data.accessToken){
				localStorage.setItem("token", data.accessToken)
				fetch('https://afternoon-inlet-64537.herokuapp.com/api/users/details', {
					headers: {
						Authorization: `Bearer ${data.accessToken}` 
					}
				})
				.then(res => res.json())
				.then(data => {
					
					
					localStorage.setItem("id", data._id)
					localStorage.setItem("email", data.email)
				
					setUser({

						id: data._id, 
						email: data.email,

					})

					Swal.fire({
						icon: "success",
						title: "Successfully Logged In.",
						text: "Thank you for logging in."
					})


					Router.push("/")
				})
			} else {
				Swal.fire({

					icon: "error",
					title: "Unsuccessful Login",
					text: "User credentials are wrong"		
				})
			}

		})

		setEmail("")
		setPassword("")



	}

	function authenticateGoogleToken(response){

		console.log(response)

		fetch('https://afternoon-inlet-64537.herokuapp.com/api/users/verify-google-id-token', {

				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({

					tokenId:response.tokenId,
					accessToken:response.accessToken
				})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)
			if(typeof data.accessToken !== 'undefined'){

				localStorage.setItem('token',data.accessToken)

				fetch('https://afternoon-inlet-64537.herokuapp.com/api/users/details', {

					headers: {

						'Authorization': `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {

					localStorage.setItem("id", data._id)
					localStorage.setItem('email', data.email)
					

					setUser({

						email: data.email,
						id: data._id
					})

					Swal.fire({

						icon: 'success',
						title: 'Succesful login'
					})

					Router.push('/')
				})
			
			} else {

				if(data.error === "google-auth-error"){

					Swal.fire({

						icon: 'error',
						title: 'Google Authenticaion Failed'
					})					
				} else if(data.error === "login-type-error"){				

					Swal.fire({

						icon: 'error',
						title: 'Login Failed',
						text: 'You may have registered through a different procedure.'

					})
				}
			}
		})
	}

	// function failed(response){
	// 	console.log(response)
	// }

	return (

		<div className={styles.container}>
			<div className={styles.main}>
				<Row>
					<Col lg={4}>
						<h1 className={styles.title}>P<img src="/gem.ico"/>cket Gem</h1>					
						<div className={styles.card}>				
						<Form onSubmit={e => authenticate(e)}>
							<Form.Group controlId="userEmail">
								<Form.Control type="email" placeholder="Email" value={email} onChange={e => setEmail(e.target.value)} required>
								</Form.Control>
							</Form.Group>
							<Form.Group controlId="userPassword">
								<Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} required>
								</Form.Control>
							</Form.Group>		
							{
								isActive
								? <Button variant="danger" className="btn-block mb-1" type="submit">Log In</Button>
								: <Button variant="danger" className="btn-block mb-1" disabled>Log In</Button>
							}
							<p className={styles.line}> or </p>
							<GoogleLogin

								clientId="348746014898-nd78bldlub1cfcatpi0bp97rrm0h5jj6.apps.googleusercontent.com" buttonText="Login Using Google" onSuccess={authenticateGoogleToken} onFailure={authenticateGoogleToken} cookiePolicy={'single_host_origin'} className="w-100 text-center my-2 d-flex justify-content-center" />
						</Form>
							<a href="./register" className="btn-block text-center"> Not yet registered? Sign Up </a>
						</div>
						<a href="./about" className="btn-block text-center"> What is Pocket Gem? </a>
					</Col>
					<Col lg={8}>
						<Carousel>
							<Carousel.Item>
								<img
								className="d-block w-100"
							    src="/transactionRecording.svg"
							    alt="Transaction Tracking"
							    />
							</Carousel.Item>
							<Carousel.Item>
								<img
							      className="d-block w-100"
							      src="/monthlyTracking.svg"
							      alt="Monthly Trend"
							   	/>
							</Carousel.Item>
							<Carousel.Item>
								<img
							      className="d-block w-100"
							      src="/percentageAnalytics.svg"
							      alt="Percentage Analytics"
							    />

							</Carousel.Item>
						</Carousel>
					</Col>
				</Row>
				<div className={styles.footer}>
					<p className="p-3 mb-0 text-center"> Genevieve Manzanilla &copy; 2021</p>
				</div>
			</div>
		</div>

		)
}