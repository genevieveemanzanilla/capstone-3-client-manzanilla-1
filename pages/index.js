import Head from 'next/head'
import {Fragment,useEffect,useState,useContext} from 'react'
import styles from '../styles/Home.module.css'
import Router from 'next/router'
import {Container,Row,Col,Form,Button} from 'react-bootstrap'

import UserContext from '../userContext'

export default function Home() {

    const {user} = useContext(UserContext)

    useEffect(()=>{

        if (user.email !== null){
            Router.push("/profile")
        } else if (user.email == null) {
            Router.push("/login")
        }

    },[])   
    
    const [email,setEmail] = useState("")
    const [message,setMessage] = useState("")    
    const [isActive,setIsActive] = useState(true)

    useEffect(()=>{

        if(email !=="" && message !==""){

            setIsActive(true)
        } else {

            setIsActive(false)
        }

    },[email,message]) 

    function sendMessage(){
        Router.push("./login")
    }



    return(

        <>

          

            <div className={styles.container}>
              <main className={styles.main}>
                <h1>About the Site</h1>

                <p className={styles.description}>
                <code className={styles.code}><b>Pocket Gem</b> is an online budget tracker</code>
                </p>
                <p className={styles.description}>
                <code className={styles.code}>created with MongoDB, Express, ReactJS, NextJS and NodeJS</code>
                </p>
                
                <Col md={8}>                    
                  <div className={styles.card}>
                    <h3 className="text-center">The Developer</h3>
                    <a className="text-center" href="https://www.linkedin.com/in/genevieve-manzanilla-34261796/"
                        target="_blank"
                        rel="noopener noreferrer">
                        <h5><b>Genevieve Manzanilla</b></h5>
                    </a>                    

                  </div>
                </Col>

         
              </main> 
            </div>         
        </> 


        )
}


    //   <footer className={styles.footer}>
    //     <a
    //       href="https://www.linkedin.com/in/genevieve-manzanilla-34261796/"
    //       target="_blank"
    //       rel="noopener noreferrer"
    //     >
    //       <h5 className="varela-font">Genevieve Manzanilla &copy; 2021</h5>
    //     </a>
    //   </footer>
    // </div>