import PieChart from '../components/PieChart'
import LineGraph from '../components/LineGraph'
import Head from 'next/head'
import styles from '../styles/Home.module.css'
import numbersWithCommas from '../helpers/commas'
import toNum from '../helpers/toNum'
import moment from 'moment'
import {Fragment,useState,useEffect,useContext} from 'react'
import {DropdownButton,Dropdown,Container,Row,Col,Form,Button,Table} from 'react-bootstrap'
import Swal from 'sweetalert2'

import Router from 'next/router'

import UserContext from '../userContext'

export default function Profile(){

	const {user} = useContext(UserContext)

	const [firstName,setFirstName] = useState("")
	const [lastName,setLastName] = useState("")
	const [balance,setBalance] = useState("")

	const [categories,setCategories]=useState([])
	const [categoriesList,setCategoriesList] = useState([])

	const [recordDescription,setRecordDescription] = useState("")
	const [recordCategory,setRecordCategory] = useState("")
	const [recordType, setRecordType] = useState("")
	const [recordAmount, setRecordAmount] = useState("")

	const [displayArr,setDisplayArr] = useState([])

	const [isActive,setIsActive] = useState(true)

	useEffect(()=>{

		if(recordAmount !=="" && recordCategory !=="" && recordType !=="" && recordDescription !==""){

			setIsActive(true)
		} else {

			setIsActive(false)
		}

	},[recordType,recordAmount,recordDescription,recordCategory])		
	

	function addRecord(e){

		e.preventDefault()



		let balanceHolder = 0

		if(recordType == "Income"){
			balanceHolder = toNum(balance) + toNum(recordAmount)
		} else if(recordType == "Expense"){
			balanceHolder = toNum(balance) - toNum(recordAmount)
		}

		fetch('https://afternoon-inlet-64537.herokuapp.com/api/users/transaction', {
			method: 'POST',
			headers: { 
				'Content-Type':'application/json',
				'Authorization':`Bearer ${(localStorage.getItem('token'))}`
			},
			body: JSON.stringify({
				id: user.id, 
				type: recordType,
				category: recordCategory,
				amount: recordAmount,
				description: recordDescription,
				balance: balanceHolder
			})
		})
		.then(res => res.json())
		.then(data => {

			fetch('https://afternoon-inlet-64537.herokuapp.com/api/users/updateBalance', {
				method: 'PUT',
				headers: { 
					'Content-Type':'application/json',
					'Authorization':`Bearer ${(localStorage.getItem('token'))}`
				},
				body: JSON.stringify({
					id: user.id, 
					balance: balanceHolder
				})
			})
			.then(res => res.json())
			.then(data => {				

				Router.reload()
			})	

		})	

	}		

	useEffect(()=>{


		fetch('https://afternoon-inlet-64537.herokuapp.com/api/users/details', {
			headers: {
				Authorization: `Bearer ${(localStorage.getItem('token'))}` 
			}
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data)
			setFirstName(data.firstName)
			setLastName(data.lastName)
			setBalance(numbersWithCommas(data.balance))
		})	
	},[])	


   	useEffect(()=>{

		fetch('https://afternoon-inlet-64537.herokuapp.com/api/users/allCategories', {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}` 
			}
		})
		.then(res => res.json())
		.then(data => {
			
			setCategories(data)
	   		const categoryOptions = []
			data.forEach(cat => {				
				if(recordType === "Expense" && cat.type === "Expense"){
					categoryOptions.push(cat)
				} else if(recordType === "Income" && cat.type === "Income"){
					categoryOptions.push(cat)
				}

			})

			setCategoriesList(categoryOptions)
			setRecordCategory("")
		})   		


   	},[recordType])


   	useEffect(()=>{

   		fetch('https://afternoon-inlet-64537.herokuapp.com/api/users/allTransactions', {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}` 
			}
		})
		.then(res => res.json())
		.then(data => {
	
			setDisplayArr(data.reverse().slice(0,5)) 
			
		})  
   	},[])   		



	const transactionTypes = [
		{
		    id: 1,
		    value: 'Income'
		}, {
		    id: 2,
		    value: 'Expense'
		}
	]

   	const transactions = transactionTypes.map(option => {
   		return(
            <option key={option.id} value={option.value}>                                   
            	{option.value}
			</option>
   		)
    })

    const categoryPlot = categoriesList.map(option => {
    	return(
	        <option key={option._id} value={option.name}>                                   
	           	{option.name}
			</option>
    	)
    })    	

    const transMap = displayArr.map(item => {

    	let variant
    	let num = numbersWithCommas(item.amount)
    	let	dates = moment(item.date).format('LLL')

   		if(item.type == "Income"){
   			variant = "text-success mb-0"
   		} else {
   			variant = "text-danger mb-0"
   		}    	

    	return(    
    		<Fragment>
    			<h5 className="mb-0">{item.description}</h5>
    			<text>{dates}</text>
    			<p align="right" className={variant}>{num}</p>
    		</Fragment>
    		)
    })

	return (
			<Container className={styles.verticalAlign}>
				<Row>
			
					<Col md={6} lg={4}>
						<div className={styles.card}>							
							<h2 className="text-center">{firstName} {lastName}</h2>
							<h3 className="text-center"><img src="/wallet.ico" width="35" height="35" alt="Balance:"/> {balance} PHP</h3>
						</div>
						<div className={styles.card}>							
							<Form onSubmit={e => addRecord(e)}>
								<Form.Group controlId="recordDescription">
									<h5 className="text-center">Record New Transaction</h5>
									<Form.Control type="text" placeholder="Description" value={recordDescription} onChange={e => setRecordDescription(e.target.value)} required/>
								</Form.Group>
								<Form.Group>								
									<Form.Control type="number" placeholder="Amount" value={recordAmount} onChange={e => setRecordAmount(e.target.value)} required/> 						
								</Form.Group>							
								<Form.Group>
									<Form.Control as="select" value={recordType} onChange={e => setRecordType(e.target.value)}> 
										<option value="" disabled>- Select Transaction Type</option>
										{transactions}
									</Form.Control>
								</Form.Group>	
								<Form.Group>								
									<Form.Control as="select" value={recordCategory} onChange={e => setRecordCategory(e.target.value)}> 
										<option value="" disabled>- Select Category</option>
										{categoryPlot}
									</Form.Control>								
								</Form.Group>
								<Row>
									<Col>
										{
										isActive
										? <Button className="working-button col-md-3" type="submit" variant="danger">Record</Button>
										: <Button variant="danger" disabled>Record</Button>
										}										
									</Col>
									<Col>
										<a href="/categories">New Category</a>
									</Col>
								</Row>
							</Form>
						</div>						

					</Col>
					<Col>						
						<div className={styles.card}>
							<h3>Recent Transactions</h3>
							{transMap}
						</div>

					</Col>						

					<Col>
						<Row>							
							<div className={styles.card}>						
								<PieChart />							
							</div>
						</Row>
						<Row>
							<Col md={6}>								
								<a href="/transactions" className="text-danger"> View All Transactions</a>				
							</Col>
							<Col md={6}>								
								<img className="mt-4" src="/bridgeProfile.svg" alt="image" height="200px"/>
							</Col>
						</Row>				

					</Col>
				</Row>			
			</Container>
	)
}

			// <DropdownButton 
			// 	title={categoryValue}
			// 	onSelect={e => selectCategory(e)}>
			// 	<Dropdown.Item eventKey="Income">Income</Dropdown.Item>
			// 	<Dropdown.Item eventKey="Expense">Expense</Dropdown.Item>
			// </DropdownButton>

					// if(data === true){
					// 	Swal.fire({

					// 		icon: "success",
					// 		title: "YAS",
					// 		text: "na add sya"
					// 	})
					// 	Router.push("/login")
					// } else {
					// 	Swal.fire({

					// 		icon: "error",
					// 		title: "nope",
					// 		text: "Something went wrong"
					// 	})
					// }			

		// let token = localStorage.getItem('token')
		// let id = user.id

		// fetch('http://localhost:8000/api/users/category', {
		// 	method: 'POST',
		// 	headers: { 
		// 		'Content-Type':'application/json',
		// 		'Authorization':`Bearer ${token}`
		// 	},
		// 	body: JSON.stringify({
		// 		id: id,
		// 		name: categoryName,
		// 		type: type
		// 	})
		// })
		// .then(res => res.json())
		// .then(data => {

		// 	console.log(data)

		// })

// const [categoryOptions, setCategoryOptions] = useState([])		

