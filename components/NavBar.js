import {Fragment,useContext} from 'react'
import {Navbar,Nav} from 'react-bootstrap'
import styles from '../styles/Home.module.css'

import Link from 'next/link'

import UserContext from '../userContext'

export default function NavBar(){

	const {user} = useContext(UserContext)

	return(
			user.email
			?
			<Navbar expand="lg" className={styles.navibar}>
				<Link href="/">
					<a className="navbar-brand inline"><h1>P<img src="/gem.ico"/>cket Gem</h1></a>
				</Link>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ml-auto">
						<Link href='/profile'>
							<a className="nav-link" role="button">Profile</a>
						</Link>													
						<Link href='/transactions'>
							<a className="nav-link" role="button">Transactions</a>
						</Link>								
						<Link href='/analytics'>
							<a className="nav-link" role="button">Analytics</a>
						</Link>	
						<Link href='/about'>
							<a className="nav-link" role="button">About</a>
						</Link>																									
						<Link href='/logout'>
							<a className="nav-link" role="button">Log Out</a>
						</Link>				
					</Nav>
				</Navbar.Collapse>
			</Navbar>
			:
			<Navbar expand="lg" className={styles.navibar}>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ml-auto">	
						<Link href='/login'>
							<a className="nav-link" role="button">Login</a>
						</Link>
						<Link href='/about'>
							<a className="nav-link" role="button">About</a>
						</Link>
					</Nav>
				</Navbar.Collapse>
			</Navbar>

		)
}