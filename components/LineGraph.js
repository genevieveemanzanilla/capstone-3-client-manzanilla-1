import { Line } from "react-chartjs-2";
import {useState,useEffect} from 'react'
import {Row,Col,Container} from 'react-bootstrap'
import moment from 'moment'

export default function LineGraph() {

  const [balance,setBalance] = useState([])
  const [transaction, setTransaction] = useState([])
  const [income,setIncome] = useState([])
  const [expense,setExpense] = useState([])

  useEffect(()=>{

    fetch('https://afternoon-inlet-64537.herokuapp.com/api/users/allTransactions',{
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {


      setTransaction(data.map(res => res.category))
      setBalance(data.map(res => res.balance))
    })
  },[])

  const Data = {

    labels: transaction,
    datasets: [{

      label: "Balance",
      data: balance,
      fill: true,
      backgroundColor: "rgba(45, 180, 150,0.2)",
      borderColor: "rgba(45, 180, 150,1)"

    }]

  }

  const options = {

    scales:{yAxes:[{ticks:{beginAtZero: true}}]}
  }
  return (
    <>
    <Container>  
      <h2 className="text-center">Budget Trend</h2>
      <Line data={Data} />
    </Container>
    </>

    )
}

// ,
//     {

//       label: "Income",
//       data: incomes
//       fill: false,
//       borderColor: "#2DB496"

//     },
//     {

//       label: "Expense",
//       data: expense
//       fill: false,
//       borderColor: "#E94435"

//     }