import {Fragment,useContext} from 'react'
import {Navbar,Nav} from 'react-bootstrap'
import styles from '../styles/Home.module.css'

import Link from 'next/link'

import UserContext from '../userContext'

export default function Footer(){

return (
		<footer className={styles.footer}>
	        <a 
	          href="/about"
	          target="_blank"
	          className="navbar-brand inline"><h5>Pocket Gem <img src="/gem.ico"/></h5></a>
	    </footer>
	)
}